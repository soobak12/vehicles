var vehicles = {
        vehicle1: {
          firstname: "Jack",
          lastname: "Davis",
          age: 25,
          id: 1
        },
        vehicle2: {
          firstname: "Mary",
          lastname: "Taylor",
          age: 37,
          id: 2
        },
        vehicle3: {
          firstname: "Peter",
          lastname: "Thomas",
          age: 17,
          id: 3
        },
        vehicle4: {
          firstname: "Peter",
          lastname: "Thomas",
          age: 17,
          id: 4
        }
      }

exports.create = function(req, res) {
  var newVehicle = req.body
    vehicles["vehicle" + newVehicle.id] = newVehicle
  console.log("--->After Post, vehicles:\n" + JSON.stringify(vehicles, null, 4))
    res.end("Post Successfully: \n" + JSON.stringify(newVehicle, null, 4))
}

exports.findAll = function(req, res) {
    console.log("--->Find All: \n" + JSON.stringify(vehicles, null, 4))
    res.end("All vehicles: \n" + JSON.stringify(vehicles, null, 4))
}

exports.findOne = function(req, res) {
    var vehicle = vehicles["vehicle" + req.params.id]
    console.log("--->Find vehicle: \n" + JSON.stringify(vehicle, null, 5))
    res.end( "Find a vehicle:\n" + JSON.stringify(vehicle, null, 4))
}

exports.update = function(req, res) {
  var id = parseInt(req.params.id)
  var updatedVehicle = req.body
  if(vehicles["vehicle" + id] != null){
    // update data
    vehicles["vehicle" + id] = updatedVehicle

    console.log("--->Update Successfully, vehicles: \n" + JSON.stringify(vehicles, null, 4))

    // return
    res.end("Update Successfully! \n" + JSON.stringify(updatedVehicle, null, 4))
  }else{
    res.end("Don't Exist vehicle:\n:" + JSON.stringify(updatedVehicle, null, 4))
  }
}

exports.delete = function(req, res) {
  var deleteVehicle = vehicles["vehicle" + req.params.id]
    delete vehicles["vehicle" + req.params.id]
    console.log("--->After deletion, vehicle list:\n" + JSON.stringify(vehicles, null, 4) )
    res.end( "Deleted vehicle: \n" + JSON.stringify(deleteVehicle, null, 4))
}
