module.exports = function(app){
  var vehicles = require('../controllers/vehicle.controller.js')

  app.post('/api/vehicles', vehicles.create)
  app.get('/api/vehicles', vehicles.findAll)
  app.get('/api/vehicles/:id', vehicles.findOne)
  app.put('/api/vehicles/:id', vehicles.update)
  app.delete('/api/vehicles/:id', vehicles.delete)
}
