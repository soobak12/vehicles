const Sequelize = require('sequelize')
const sequelize = new Sequelize('vehicles','gocarapp','xptmxm12#', {
  host: '192.168.0.100',
  dialect: 'mariadb'
})

const Vehicle = sequelize.define('car_detail_mst', {
  vehicle_code: Sequelize.STRING(10),
  vehicle_name: Sequelize.STRING(100),
  group_code  : Sequelize.STRING(5),
  fuel_code   : Sequelize.STRING(5),
  body_code   : Sequelize.STRING(10),
  appr_code   : Sequelize.STRING(5),
  color_code  : Sequelize.STRING(10),
  price       : Sequelize.INTEGER,
  rel_date    : Sequelize.STRING(8),
  displacement: Sequelize.INTEGER,
  drive_kind  : Sequelize.STRING(10),
  capacity    : Sequelize.INTEGER,
  regr_id     : Sequelize.STRING(50),
  updr_id     : Sequelize.STRING(50)

})

module.exports = {
  sequelize: sequelize,
  Vehicle: Vehicle
}
