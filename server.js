var express = require('express')
var app = express()
var bodyParser = require('body-parser')
app.use(bodyParser.json())

require('./app/routes/vehicle.routes.js')(app)
var server = app.listen(3030, function(){
  var host = server.address().address
  var port = server.address().port

  require('./app/models/vehicle.model.js').sequelize.sync({force: false})
    .then(() => {
      console.log('Database Sync')
    })

  console.log("App listening at http://%s:%s", host, port)
})
